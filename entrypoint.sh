#!/bin/bash

airflow db init

airflow users create \
    --username admin \
    --firstname John \
    --lastname Doe \
    --role Admin \
    --email admin@example.com \
    --password admin


exec airflow webserver