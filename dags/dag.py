import requests
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from sqlalchemy import create_engine, String, Integer, Float, MetaData, Table, Column, insert
from sqlalchemy.dialects.postgresql import insert as pg_insert
import pandas as pd

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

def dtype_to_sqltype(dtype):
    if "int" in str(dtype):
        return Integer
    elif "float" in str(dtype):
        return Float
    else:
        return String

def fetch_and_store_data():
    # Получение данных из API
    url = "https://random-data-api.com/api/cannabis/random_cannabis?size=10"
    response = requests.get(url)
    data = response.json()
    df = pd.DataFrame(data)

    engine = create_engine("postgresql+psycopg2://airflow:airflow@postgres:5432/postgres")

    # Проверка наличия таблицы. Если ее нет, создаем.
    metadata = MetaData()
    if not engine.dialect.has_table(engine, "cannabis"):
        columns = [Column(name, dtype_to_sqltype(dtype)) for name, dtype in df.dtypes.items()]
        table = Table('cannabis', metadata, *columns)
        table.create(bind=engine)
    else:
        table = Table('cannabis', metadata, autoload=True, autoload_with=engine)

    # Upsert данных
    data_to_insert = df.to_dict(orient='records')
    stmt = pg_insert(table).values(data_to_insert).on_conflict_do_nothing()
    with engine.connect() as conn:
        conn.execute(stmt)

with DAG('cannabis_to_postgres_dag',
         default_args=default_args,
         description='Получение данных о каннабисе и сохранение их в PostgreSQL',
         schedule_interval=timedelta(hours=12),  # Запуск каждые 12 часов
         start_date=datetime(2023, 10, 27),
         catchup=False) as dag:

    fetch_and_store_task = PythonOperator(
        task_id='fetch_and_store',
        python_callable=fetch_and_store_data,
    )
